import React, { Component } from 'react';
import iconHome from './icon/home-active.png';
import iconOrder from './icon/order.png';

// if image from url can use source{url:'nama_url'} 
// fontWeight for make bold style
// flex-end untuk membuat suatu view rata kanan dan wrap_content 
// borderBottomColor make buttom border
// borderTopLeftRadius make radius in corner  
// TouchhableOpacity make response when bottom touch 
// width undefined untuk membuat suatu view tidak dapat terdefinisi 
// image cover digunakan untuk membuat suatu image besarnya akan memenuhi suatu viewjadi tidak perlu medifinisikan width image

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  TextInput, 
  TouchableOpacity
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

export default class App extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        {/* memakan sisa tinggi yang ada */}
        {/* view pager */}
        <ScrollView style={{ flex: 1, backgroundColor: 'white' }}>
          {/* search bar */}
          <View style={{ marginHorizontal: 17, marginTop: 15, flexDirection: 'row' }}>
            <View style={{ position: 'relative', flex: 1 }}>
              <TextInput placeholder="what do you want to eat?" style={{ borderWidth: 1, borderColor: '#EBEBEB', borderRadius: 25, height: 40, fontSize: 13, paddingLeft: 45, paddingRight: 20, backgroundColor: 'white', marginRight: 18 }} />
              <Image source={require('./icon/search.png')} style={{ position: 'absolute', top: 5, left: 12 }} />
            </View>
            <View style={{ width: 35, alignItems: 'center', justifyContent: 'center' }}>
              <Image source={require('./icon/promo.png')} />
            </View>
          </View>
          {/* gopay  */}
          <View style={{ marginHorizontal: 17, marginTop: 8 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#2c5fb8', borderTopLeftRadius: 4, borderTopRightRadius: 4, padding: 14 }}>
              <Image source={require('./icon/gopay.png')} />
              <Text style={{ fontSize: 17, fontWeight: 'bold', color: 'white' }}>Rp. 50.000</Text>
            </View>
            <View style={{ flexDirection: 'row', paddingTop: 20, paddingBottom: 14, backgroundColor: '#2f65bd', borderBottomLeftRadius: 4, borderBottomRightRadius: 4 }}>
              <View style={{ flex: 1, alignItems: 'center' }}>
                <Image source={require('./icon/pay.png')} />
                <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'white', marginTop: 15 }}>Pay</Text>
              </View>
              <View style={{ flex: 1, alignItems: 'center' }}>
                <Image source={require('./icon/nearby.png')} />
                <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'white', marginTop: 15 }}>Nearby</Text></View>
              <View style={{ flex: 1, alignItems: 'center' }}>
                <Image source={require('./icon/topup.png')} />
                <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'white', marginTop: 15 }}>Top Up</Text>
              </View>
              <View style={{ flex: 1, alignItems: 'center' }}>
                <Image source={require('./icon/more.png')} />
                <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'white', marginTop: 15 }}>More</Text>
              </View>
            </View>
          </View>
          {/* main feature */}
          <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 16 }}>
            <View style={{ justifyContent:'space-between', flexDirection: 'row', width:'100%', marginBottom:18}}>
              <View style={{width:'25%', alignItems:'center'}}>
                <View style={{ width: 58, height: 58, borderWidth: 1, borderColor: '#EFEFEF', borderRadius: 18, justifyContent:'center', alignItems:'center' }}>
                  <Image source={require('./icon/go-ride.png')}/>
                </View>
                <Text style={{fontSize:11, fontWeight:'bold', textAlign:'center', marginTop:6}}>Go-RIDE</Text>
              </View>  
              <View style={{width:'25%', alignItems:'center'}}>
                <View style={{ width: 58, height: 58, borderWidth: 1, borderColor: '#EFEFEF', borderRadius: 18, justifyContent:'center', alignItems:'center' }}>
                  <Image source={require('./icon/go-car.png')}/>
                </View>
                <Text style={{fontSize:11, fontWeight:'bold', textAlign:'center', marginTop:6}}>Go-CAR</Text>
              </View>  
              <View style={{width:'25%', alignItems:'center'}}>
                <View style={{ width: 58, height: 58, borderWidth: 1, borderColor: '#EFEFEF', borderRadius: 18, justifyContent:'center', alignItems:'center' }}>
                  <Image source={require('./icon/go-bluebird.png')}/>
                </View>
                <Text style={{fontSize:11, fontWeight:'bold', textAlign:'center', marginTop:6}}>Go-BLUEBIRD</Text>
              </View>  
              <View style={{width:'25%', alignItems:'center'}}>
                <View style={{ width: 58, height: 58, borderWidth: 1, borderColor: '#EFEFEF', borderRadius: 18, justifyContent:'center', alignItems:'center' }}>
                  <Image source={require('./icon/go-send.png')}/>
                </View>
                <Text style={{fontSize:11, fontWeight:'bold', textAlign:'center', marginTop:6}}>Go-SEND</Text>
              </View>  
            </View>
            <View style={{ justifyContent:'space-between', flexDirection: 'row', width:'100%', marginBottom:18}}>
              <View style={{width:'25%', alignItems:'center'}}>
                <View style={{ width: 58, height: 58, borderWidth: 1, borderColor: '#EFEFEF', borderRadius: 18, justifyContent:'center', alignItems:'center' }}>
                  <Image source={require('./icon/go-deals.png')}/>
                </View>
                <Text style={{fontSize:11, fontWeight:'bold', textAlign:'center', marginTop:6}}>Go-DEALS</Text>
              </View>  
              <View style={{width:'25%', alignItems:'center'}}>
                <View style={{ width: 58, height: 58, borderWidth: 1, borderColor: '#EFEFEF', borderRadius: 18, justifyContent:'center', alignItems:'center' }}>
                  <Image source={require('./icon/go-pulsa.png')}/>
                </View>
                <Text style={{fontSize:11, fontWeight:'bold', textAlign:'center', marginTop:6}}>Go-PULSA</Text>
              </View>  
              <View style={{width:'25%', alignItems:'center'}}>
                <View style={{ width: 58, height: 58, borderWidth: 1, borderColor: '#EFEFEF', borderRadius: 18, justifyContent:'center', alignItems:'center' }}>
                  <Image source={require('./icon/go-food.png')}/>
                </View>
                <Text style={{fontSize:11, fontWeight:'bold', textAlign:'center', marginTop:6}}>Go-FOOD</Text>
              </View>  
              <View style={{width:'25%', alignItems:'center'}}>
                <View style={{ width: 58, height: 58, borderWidth: 1, borderColor: '#EFEFEF', borderRadius: 18, justifyContent:'center', alignItems:'center' }}>
                  <Image source={require('./icon/go-more.png')}/>
                </View>
                <Text style={{fontSize:11, fontWeight:'bold', textAlign:'center', marginTop:6}}>Go-MORE</Text>
              </View>  
            </View>
          </View>
          {/* slashing */}
          <View style={{height:17, width:'100%', backgroundColor:'#F2F2F4', marginTop:20}}></View> 
          {/* new  */}
          <View style={{paddingTop:16, paddingLeft:16, paddingRight:16}}> 
            <View style={{position:'relative'}}> 
              <Image source={require('./dummy/sepak-bola.jpg')} style={{height:200, width:'100%', borderRadius:6}}/>
              <View style={{width:'100%', height:'100%', position:'absolute', top:0, left:0, backgroundColor:'black', opacity:0.3, borderRadius:6}}> 
              </View>  
              <Image source={require('./logo/white.png')} style={{height:15, width: 55, position:'absolute', top:16, left:16}}/>  
            </View> 
            <View style={{paddingTop:16, paddingBottom:20, borderBottomColor:'#E8E9ED', borderBottomWidth:1, marginBottom:20}}> 
              <Text style={{fontSize:16, fontWeight: 'bold', color:'#1c1c1f'}}> 
                GO-NEW
              </Text> 
              <Text style={{fontSize:14, fontWeight:'normal', color:'#7A7A7A'}}>Dimas Drajat selamatkan penalti, Timnas U-23 kalahkan Brunei Darussalam </Text> 
              <TouchableOpacity style={{backgroundColor:'#61A756', paddingHorizontal:12, paddingVertical:11, alignSelf:'flex-end', borderRadius: 4,  marginTop:10}}>
                <Text style={{fontSize:12,fontWeight:'bold', color:'white', textAlign:'center'}}>READ</Text>
              </TouchableOpacity>
            </View>
          </View> 
          {/* facebook  */}
          <View style={{paddingLeft:16, paddingRight:16, paddingTop:16}}> 
              <Image source={require('./logo/gojek.png')} style={{height:15, width:55}}/> 
              <Text style={{fontSize:17, fontWeight:'bold',color:'#1c1c1c', marginTop:15, marginBottom:15}}>Complete your profile</Text> 
              <View style={{flexDirection:'row', marginBottom:20}}> 
                <View>
                  <Image source={require('./dummy/facebook-connect.png')}/>
                </View> 
                <View style={{marginLeft:11, flex:1}}> 
                  <Text style={{fontSize:15, fontWeight:'bold',color:'#4A4A4A'}}>Connect with Facebook</Text> 
                  <Text style={{fontSize:15, color:'#4A4A4A', width:'70%'}}>Login faster without verification code</Text>
                </View> 
              </View> 
              <TouchableOpacity style={{backgroundColor:'#61A756', paddingHorizontal:12, paddingVertical:11, alignSelf:'flex-end', borderRadius: 4, marginTop:10}}>
                    <Text style={{fontSize:10, fontWeight:'bold', color:'white', textAlign:'center'}}>CONNECT</Text>
              </TouchableOpacity> 
              <View style={{borderBottomColor:'#E8E9ED', borderBottomWidth:1, marginTop:20}}></View>
          </View>
          {/* voucher */}
          <View style={{paddingTop:16, paddingLeft:16, paddingRight:16, marginBottom:16}}>
            <View style={{position:'relative'}}> 
              <Image source={require('./dummy/food-banner.jpg')} style={{height:200, width:'100%', borderRadius:6}}/> 
              <View style={{width:'100%', height:'100%', position:'absolute', top:0, left:0, backgroundColor:'black', opacity:0.1, borderRadius:6}}></View> 
              <Image source={require('./logo/white.png')} style={{height:15, width: 55, position:'absolute', top:16, left:16}}/>  
              <View style={{position:'absolute', bottom:0, left:0, width:'100%', flexDirection:'row', alignItems:'center', paddingLeft:16, paddingRight:16, paddingBottom:16}}> 
                <View>
                  <Text style={{fontSize:18, fontWeight:'bold', color:'white', marginBottom:3}}>Free Go-FOOD voucher</Text> 
                  <Text style={{fontSize:12, fontWeight:'500', color:'white'}}>Quick, before then run put!</Text>
                </View>
                <View style={{flex:1, paddingLeft:12}}>
                  <TouchableOpacity style={{backgroundColor:'#61A756', paddingHorizontal:12, paddingVertical:11,  borderRadius: 4}}>
                    <Text style={{fontSize:10, fontWeight:'bold', color:'white', textAlign:'center'}}>GET VOUCHER</Text>
                  </TouchableOpacity>
                </View>  
              </View>  
            </View>
            <View style={{borderBottomColor:'#E8E9ED', borderBottomWidth:1, marginTop:20, marginBottom:6}}></View>  
          </View>
          <View> 
            <Image source={require('./logo/go-food.png')} style={{height:15, marginLeft:16, marginBottom:16}}/> 
            <View style={{flexDirection:'row', justifyContent: 'space-between', marginBottom:16, paddingLeft:16, paddingRight:16}}> 
              <Text style={{fontSize:17, fontWeight:'bold', color:'#1c1c1c'}}>Nearby Restaurant</Text> 
              <Text style={{fontSize:17, color:'#61A756', fontWeight:'bold'}}>See All</Text>  
            </View> 
            <ScrollView horizontal={true} style={{flexDirection:'row', paddingLeft:16, paddingRight:16}}> 
              <View> 
                <View style={{width:150, height:150, backgroundColor:'pink', borderRadius:4}}> 
                  <Image source={require('./dummy/go-food-kfc.jpg')} style={{height:undefined, width:undefined, resizeMode:'cover', flex:1, borderRadius:4}}/> 
                </View> 
                <View><Text style={{fontSize:16, fontWeight:'bold', color:'#1c1c1c', marginTop:12}}>KFC Aeon Mall</Text></View>  
              </View> 
              <View style={{marginLeft:16}}> 
                <View style={{width:150, height:150, backgroundColor:'pink', borderRadius:4}}> 
                  <Image source={require('./dummy/go-food-gm.jpg')} style={{height:undefined, width:undefined, resizeMode:'cover', flex:1, borderRadius:4}}/> 
                </View> 
                <View><Text style={{fontSize:16, fontWeight:'bold', color:'#1c1c1c', marginTop:12}}>Bakmi GM Aeon Mall</Text></View>  
              </View>   
              <View style={{marginLeft:16}}> 
                <View style={{width:150, height:150, backgroundColor:'pink', borderRadius:4}}> 
                  <Image source={require('./dummy/go-food-orins.jpg')} style={{height:undefined, width:undefined, resizeMode:'cover', flex:1, borderRadius:4}}/> 
                </View> 
                <View><Text style={{fontSize:16, fontWeight:'bold', color:'#1c1c1c', marginTop:12}}>Martabak Orins</Text></View>  
              </View>   
              <View style={{marginLeft:16}}> 
                <View style={{width:150, height:150, backgroundColor:'pink', borderRadius:4}}> 
                  <Image source={require('./dummy/go-food-banka.jpg')} style={{height:undefined, width:undefined, resizeMode:'cover', flex:1, borderRadius:4}}/> 
                </View> 
                <View><Text style={{fontSize:16, fontWeight:'bold', color:'#1c1c1c', marginTop:12}}>Martabak Banka</Text></View>  
              </View>   
              <View style={{marginLeft:16, marginRight:16}}> 
                <View style={{width:150, height:150, backgroundColor:'pink', borderRadius:4}}> 
                  <Image source={require('./dummy/go-food-pak-boss.jpg')} style={{height:undefined, width:undefined, resizeMode:'cover', flex:1, borderRadius:4}}/> 
                </View> 
                <View><Text style={{fontSize:16, fontWeight:'bold', color:'#1c1c1c', marginTop:12}}>Ayam Bakar Pak Boss</Text></View>  
              </View>    
            </ScrollView> 
            <View style={{borderBottomColor:'#E8E9ED', borderBottomWidth:1, marginTop:20, marginBottom:6, marginLeft:16, marginRight:16}}></View> 
          </View>
        </ScrollView>
        {/* tab */}
        <View style={{ height: 54, flexDirection: 'row', backgroundColor: 'white', borderTopColor:'#E8E9ED', borderTopWidth:1 }}>
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Image style={{ width: 26, height: 26 }} source={iconHome} />
            <Text style={{ fontSize: 10, color: '#545454', marginTop: 4, color: '#43AB4A' }}>Home</Text>
          </View>
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Image style={{ width: 26, height: 26 }} source={iconOrder} />
            <Text style={{ fontSize: 10, color: '#545454', marginTop: 4 }}>Orders</Text>
          </View>
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Image style={{ width: 26, height: 26 }} source={require('./icon/help.png')} />
            <Text style={{ fontSize: 10, color: '#545454', marginTop: 4 }}>Help</Text>
          </View>
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Image style={{ width: 26, height: 26 }} source={require('./icon/inbox.png')} />
            <Text style={{ fontSize: 10, color: '#545454', marginTop: 4 }}>Inbox</Text>
          </View>
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Image style={{ width: 26, height: 26 }} source={require('./icon/account.png')} />
            <Text style={{ fontSize: 10, color: '#545454', marginTop: 4 }}>Account</Text>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({

});
